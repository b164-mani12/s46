import { useState, useEffect } from 'react';
import { Form, Button} from 'react-bootstrap';

export default function Login() {

		// State hooks to store the values of the input fields
		const [email, setEmail] = useState("");
	    const [password, setPassword] = useState("");
	    // State to determine whether submit button is enabled or not
	    const [isActive, setIsActive] = useState(true);


	function authenticate(e) {

		e.preventDefault()

		setEmail("");
		setPassword("");

		/*
		Syntax:
			localStorage.setItem("propertyName", value)

		*/
		localStorage.setItem("email", email)

		alert(`${email} has been verified. Welcome back!`);
	}


	useEffect(()=> {
		//Validation to enable the submit button when all the input fields are populated and both passwords match 
		if(email !== "" && password !== "") {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password])

	return(

		<Form onSubmit={(e) => authenticate(e)}>
		<h1 className="text-center">Login</h1>
		  <Form.Group className="mb-3" controlId="userEmail">
		    <Form.Label>Email address</Form.Label>
		    <Form.Control 
		    	type="email" 
		    	placeholder="Enter email"
		    	value={email}
		    	onChange={e =>setEmail(e.target.value)} 
		    	required
		    />
		  </Form.Group>

		  <Form.Group className="mb-3" controlId="password">
		    <Form.Label>Password</Form.Label>
		    <Form.Control 
		    	type="password" 
		    	placeholder="Password" 
		    	value={password}
		    	onChange={e => setPassword(e.target.value)}
		    	required
		    />
		  </Form.Group>
		
		  {
		  	isActive ?
		  	<Button variant="success" type="submit" id="submitBtn">
		  	  Submit
		  	</Button>
		  	:
		  	<Button variant="danger" type="submit" id="submitBtn" disabled>
		  	  Submit
		  	</Button>

		  }
		</Form>

		)
}